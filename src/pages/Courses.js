// import coursesData from '../data/courses';
//comment out the importation of the mock database
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';

export default function Courses(){

	// console.log(coursesData)

	//using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	//map returns an array, which we can display in the page via the component function's return statement

	//props are a way to pass any valid JS data from parent component to child component.

	//You can pass as many props as you want. Even functions can be passed as props.

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] =useState([])

	//***role***
	const {user} = useContext(UserContext);
	// console.log(user);

	//we will create a "useEffect" hook and a fetch request that will fetch all of the courses data to be used in the "Courses" page
	useEffect(()=>{
		//***all active for students
	fetch(`${process.env.REACT_APP_API_URL}/courses`)
	.then(res=>res.json())
	.then(data=>{

		//mapping of the data inside the useEffect hook
	const courseArr = (data.map(course => {
		// console.log(course)
		return (
			/*Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components*/
			<CourseCard courseProp={course} key={course._id}/>
			)
		}))
		setCourses(courseArr)
	})

	},[])

	//***add a way to /admin
	return(
		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}